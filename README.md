# Proj0-Hello
-------------

Trivial project to exercise version control, turn-in, and other
mechanisms.

This modifies the credentials (private) file with information specific to the 
developer and corresponding configurations for the execution platform. In other words,
these resources are meant to be private, and are needed to run this project with specific output. 


# Contributors
--------------

- Ronny Fuentes <ronnyf@uoregon.edu>
